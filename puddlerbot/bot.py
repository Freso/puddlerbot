# PuddlerBot - a bot to assist with puddling business
# Copyright © 2022 Frederik “Freso” S. Olesen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
""""""

import functools
from typing import Union

import praw
import pyyoutube

YT_API = ''  # Add your YouTube API key here
YOUTUBE_ID = 'UCTb5oiMqBYdbpVT5QiX0Deg'
YT_UPLOADS_ID = 'UUTb5oiMqBYdbpVT5QiX0Deg'
SUBREDDIT = 'BadBunnyTwitch'


@functools.cache
def get_reddit_instance() -> praw.reddit.Reddit:
    return praw.Reddit('PuddlerBot',
                       user_agent='script:PuddlerBot:v0.0.0-prealpha (by u/Freso)')


@functools.cache
def get_youtube_instance() -> pyyoutube.api.Api:
    return pyyoutube.Api(api_key=YT_API)


def get_recent_videos(count: int = 2) -> list[str]:
    new_uploads = get_youtube_instance().get_playlist_items(playlist_id=YT_UPLOADS_ID, count=count,
                                                            parts="contentDetails,status").items
    new_uploads.reverse()

    video_ids = list()
    for video in new_uploads:
        if video.status.privacyStatus == 'public':
            video_ids.append(video.contentDetails.videoId)

    return video_ids


def submit_video_to_reddit(video_id: str) -> Union[praw.reddit.Submission, None]:
    video = get_youtube_instance().get_video_by_id(video_id=video_id,
                                                   parts='contentDetails,snippet,status,topicDetails').items[0]
    if not video.status.privacyStatus == 'public':
        return

    submission_url = 'https://www.youtube.com/watch?v={}'.format(video_id)

    subreddit = get_reddit_instance().subreddit(SUBREDDIT)
    return subreddit.submit(title=video.snippet.title,
                            url=submission_url,
                            resubmit=False)
